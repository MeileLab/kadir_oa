#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<string.h>

/* model parameters */
#define NPAR 7
static double parms[NPAR];

#define K_CO2 parms[0]
#define K_HCO3 parms[1]
#define K_BOH3 parms[2]
#define K_H2S parms[3]
#define K_NH4 parms[4]
#define K_w parms[5]
#define TB parms[6]

#define low_pH 6.0
#define high_pH 9.0
double rootfunc_pH(double * x, double * TAval, double * TCval, double * TSval, double * TNval) {
  double h = x[0];
  double err = (K_CO2 * h + 2 * K_CO2 * K_HCO3)  / (h*h + K_CO2 * h + K_CO2 *
                                                        K_HCO3) * TCval[0] + K_BOH3 / (K_BOH3 + h) * TB + K_H2S / (K_H2S + h) *
    TSval[0] + K_NH4 / (K_NH4 + h) * TNval[0] + K_w / h - h - TAval[0];
  return(fabs(err));
}

double goldratio(double * TAval, double * TCval, 
                   double * TSval, double * TNval) {
  const double r = 0.618033988750;
  const double tol = 1e-10;
  double a = pow(10.0, - low_pH) * 1.0e3;
  double b = pow(10.0, - high_pH) * 1.0e3;
  double x,y,u,v;
  int i;
  
  //initial step
  x = a + r * (b - a);
  y = a + r*r*(b - a);
  u = rootfunc_pH(&x, TAval, TCval, TSval, TNval);
  v = rootfunc_pH(&y, TAval, TCval, TSval, TNval);
  
  for (i = 0; i < 1e4; i++) {
    if (v < u) {
      b = x;
      x = y;
      u = v;
      y = a + r * r * (b-a);
      v = rootfunc_pH(&y, TAval, TCval, TSval, TNval);
      if (v < tol) {
        double pH = -log10(y*1e-3);
        return(pH);
      }
    } else {
      a = y;
      y = x;
      v = u;
      x = a + r * (b*b - a*a)/(b+a);
      u = rootfunc_pH(&x, TAval, TCval, TSval, TNval);
      if (u < tol) {
        double pH = -log10(x*1e-3);
        return(pH);
      }
    }
  }
  
  return(-1.0); 
}

void calc_pH(double * TA, double * TC, double * TS, double * TN, double * pH) {
  pH[0] = goldratio(TA, TC, TS, TN);
}

void calc_pHfromR(double * TA, double * TC, double * TS, double * TN, double * pH, double * parameters) {
  //copy parameters to global static scope
  for (int i_par = 0; i_par < NPAR; ++i_par) {
    parms[i_par] = (double) parameters[i_par];
  }
  
  calc_pH(TA, TC, TS, TN, pH);
}



void calc_pHvec(double * parameters, double * TA, double * TC, double * TS, double * TN, double * pH, int * n_cells) {
  //initialize acid-base constants
  for (int i_par = 0; i_par < NPAR; ++i_par) {
    parms[i_par] = (double) parameters[i_par];
  }

  for (int i_cell = 0; i_cell < *n_cells; ++i_cell) {
    pH[i_cell] = goldratio(&TA[i_cell], &TC[i_cell], &TS[i_cell], &TN[i_cell]);
  }	
}


int main(void) {
  printf("main program");
}
