This folder contains a 1-D reactive transport model for coastal sediment alkalinization.
Kadir Bice, 2024

# Description of the model implementation

Model solves 1-D advection-diffusion equations for solutes and solids considering early diagenetic processes.
More information can be found in publication: Bice et al. (2024).

## State variables: O2, SO4, NO3, TS, DIC, TA, Ca, Mg, Fe, NH4, CaCO3, Fe(OH)3, FeS

## Implementation: 
- Spin-up simulation for 200 years and then 2(or 50) years for mineral addition scenario. 
- Outputs include depth/time figures of each state variable and other variables of interest (e.g., pH, omega)

## Model is to set to run through terminal input paramters in order: RC0 (numeric), Fe Flux (umol cm2 yr-1), mixed layer depth (numeric, cm), mineral type (character)

### Notes for C implementation:
- Script needs to run with the compiled ph calculation C program (ph_calc.so, after work of Rooze et al., 2020) in the same directory. 
If you change machines, you might need to re-compile C code. 
More information on using C with R can be found here: https://jbhender.github.io/Stats506/F17/Using_C_Cpp.html
